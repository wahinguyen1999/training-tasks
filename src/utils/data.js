export const statusType = {
  New: 0,
  Inprogress: 1,
  Done: 2,
};

export const statusOptions = [
  {
    value: 0,
    label: "New",
  },
  {
    value: 1,
    label: "In-Progress",
  },
  {
    value: 2,
    label: "Done",
  },
];

export const listPlanType = [
  {
    value: "",
    label: "Please select",
  },
  {
    value: 1,
    label: "Type 1",
  },
  {
    value: 2,
    label: "Type 2",
  },
  {
    value: 3,
    label: "Type 3",
  },
];
