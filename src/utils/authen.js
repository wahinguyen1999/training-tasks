import { AuthStorage } from "./constants.js";

function IsAuthentication(to, from, next) {
  var auth = localStorage.getItem(AuthStorage);
  if (!auth) {
    next("/login");
  }
  next();
}

export default IsAuthentication;
