export function getLocalStorage(item) {
  var data = localStorage.getItem(item);
  if (!data) return null;
  return JSON.parse(data);
}

export function setLocalStorage(item, data) {
  return localStorage.setItem(item, JSON.stringify(data));
}

export function removeLocalStorage(item) {
  return localStorage.removeItem(item);
}

export function generateCorePagination(currentPageCore, totalPages) {
  // [minPage, ..., startPage, endPage, ..., maxPage]
  const minPage = 1;
  const maxPage = totalPages;
  const hasDot = maxPage > 4 ? true : false;
  let startPage = minPage;
  let endPage = maxPage;
  let result = [];

  if (hasDot) {
    if (currentPageCore - minPage == 0) {
      startPage = currentPageCore + 1;
      endPage = currentPageCore + 2;
    } else if (currentPageCore - minPage == 1) {
      startPage = currentPageCore;
      endPage = currentPageCore + 2;
    } else if (currentPageCore - minPage == 2) {
      startPage = currentPageCore - 1;
      endPage =
        currentPageCore + 2 == maxPage
          ? currentPageCore + 1
          : currentPageCore + 2;
    } else if (
      currentPageCore - minPage > 2 &&
      currentPageCore - maxPage <= -3
    ) {
      startPage = currentPageCore - 2;
      endPage = currentPageCore + 2;
    } else if (currentPageCore - maxPage == -2) {
      startPage = currentPageCore - 2;
      endPage = currentPageCore + 1;
    } else if (currentPageCore - maxPage == -1) {
      startPage = currentPageCore - 2;
      endPage = currentPageCore;
    } else if (currentPageCore - maxPage == 0) {
      startPage = currentPageCore - 2;
      endPage = currentPageCore - 1;
    }
  }

  if (hasDot) {
    result.push(minPage);
    if (startPage >= 3) {
      result.push("...");
    }
  }

  for (var i = startPage; i <= endPage; i++) {
    result.push(i);
  }

  if (hasDot) {
    if (endPage <= maxPage - 2) {
      result.push("...");
    }
    result.push(maxPage);
  }

  return result;
}

export function startParentLoading() {
  const el = document.getElementById("app");
  el.classList.add("loading");
}

export function stopParentLoading() {
  const el = document.getElementById("app");
  el.classList.remove("loading");
}

export function startLoading(id) {
  const item = document.getElementById(id);
  item.classList.add("loading");
}

export function stopLoading(id) {
  const item = document.getElementById(id);
  item.classList.remove("loading");
}
