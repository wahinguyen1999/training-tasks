export const AuthStorage = "Auth";

export const Role = {
  Admin: 0,
  User: 1,
};

export const Navigates = [
  {
    name: "Home",
    originPath: "/home",
    path: "/home",
    role: [Role.Admin, Role.User],
    icon: "fa-solid fa-house",
  },
  {
    name: "Plan",
    originPath: "/plan",
    path: "/plan/list",
    role: [Role.Admin],
    icon: "fa-solid fa-bars-progress",
  },
  {
    name: "Contract",
    originPath: "/contract",
    path: "/contract/list",
    role: [Role.Admin],
    icon: "fa-solid fa-file-contract",
  },
];
