import httpService from "../plugins/httpService.js";
import { mapMutations } from "vuex";

var fetchListMixin = {
  data: function () {
    return {
      page: 1,
      total: 0,
      items: [],
      isLoading: false,
    };
  },
  computed: {},
  watch: {
    page(newVal) {
      this.query.page = newVal.toString();
      this.$router.push({
        path: this.pathList,
        query: this.query,
      });
    },
    async $route() {
      if (Object.keys(this.$route.query).length === 0) {
        this.reset();
        await this.getList(this.apiEndPoint, {});
      }
    },
  },
  async created() {
    const listQueryKeyParams = Object.keys(this.query || {});

    // Update query from url if already exist
    if (listQueryKeyParams.length > 0) {
      for (var param of listQueryKeyParams) {
        if (param == "page") {
          this.query[param] = this.$route.query[param] || "1";
        } else {
          this.query[param] = this.$route.query[param] || "";
        }
      }
    }

    await this.getList(this.apiEndPoint, this.query);
  },
  methods: {
    ...mapMutations(["SET_SEARCH_QUERY"]),
    isQueryChanged() {
      const urlQueryString = mapQueryString(this.$route.query);
      const queryString = mapQueryString(this.query);

      if (urlQueryString != queryString) return true;

      return false;

      function mapQueryString(query) {
        return Object.keys(query)
          .map(
            (key) =>
              `${encodeURIComponent(key)}=${encodeURIComponent(query[key])}`
          )
          .join("&");
      }
    },
    reset(page) {
      this.initialQuery(page);
    },
    async search() {
      if (this.isQueryChanged() == true) {
        this.$router.push({ path: this.$route.path, query: this.query });
        await this.getList(this.apiEndPoint, this.query);
      }
    },
    async getList(url, query) {
      try {
        this.isLoading = true;
        const response = await httpService.get(url, query);

        if (response.httpCode !== 200) {
          return;
        }

        this.items = response.data.data;
        this.page = response.data.current_page;
        this.total = response.data.total;
      } catch (error) {
        console.error("Error in getList:", error);
      } finally {
        this.isLoading = false;
        this.SET_SEARCH_QUERY(this.$route.fullPath);
      }
    },
    goToEdit(item, url) {
      this.$router.push(`${url}/${item.id}`);
    },
    async changeOptions({ page }) {
      this.query.page = page.toString();

      if (this.isQueryChanged()) {
        this.reset(page);
      }

      await this.getList(this.apiEndPoint, this.query);
    },
    initialQuery(page) {
      const listQueryKeyParams = Object.keys(this.query || {});

      // Check if cannot find query
      if (listQueryKeyParams.length == 0) return false;

      listQueryKeyParams.forEach((param) => {
        if (param == "page") {
          this.query[param] = page || "1";
        } else {
          this.query[param] = this.$route.query[param] || "";
        }
      });
    },
  },
};

export default fetchListMixin;
