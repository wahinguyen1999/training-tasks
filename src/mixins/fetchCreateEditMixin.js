import httpService from "../plugins/httpService.js";
import { mapState } from "vuex";

var fetchCreateEditMixin = {
  data() {
    return {};
  },
  computed: {
    ...mapState(["searchQuery"]),
  },
  async created() {
    // Check if url has parameters and action is 'Edit'
    if (this.action === "Edit" && this.$route.params) {
      this.body.id = this.$route.params.id;
      await this.get(this.$route.params.id);
    }
  },
  methods: {
    async get(id) {
      var response = await httpService.get(`${this.apiEndPoint}/${id}`);

      if (response.httpCode != 200) {
        return;
      }

      // Map data by key body
      if (response.data) {
        const listKeyBody = Object.keys(this.body || {});

        if (listKeyBody.length > 0) {
          for (var key of listKeyBody) {
            this.body[key] = response.data[key];
          }
        }
      }
    },
    async submit() {
      switch (this.action) {
        case "Create":
          await this.create();
          break;
        case "Edit":
          await this.edit();
          break;
      }
    },
    async create() {
      try {
        var response = await httpService.post(this.apiEndPoint, this.body);

        if (response.httpCode != 200) {
          return;
        }

        this.$router.push(this.pathList);
      } catch (error) {
        console.error("Error in submit:", error);
      } finally {
        console.log("finally");
      }
    },
    async edit() {
      try {
        var response = await httpService.put(
          `${this.apiEndPoint}/${this.body.id}`,
          this.body
        );

        if (response.httpCode != 200) {
          return;
        }

        this.$router.push(this.pathList);
      } catch (error) {
        console.error("Error in edit:", error);
      } finally {
        console.log("finally");
      }
    },
    cancel() {
      if (this.searchQuery) {
        this.$router.push(this.searchQuery);
        return;
      }
      this.$router.push(this.pathList);
    },
    async deleteData() {
      try {
        var response = await httpService.delete(
          `${this.apiEndPoint}/${this.body.id}`
        );

        if (response.httpCode != 200) {
          return;
        }

        this.$router.push(this.pathList);
      } catch (error) {
        console.error("Error in delete:", error);
      } finally {
        console.log("finally");
      }
    },
  },
};

export default fetchCreateEditMixin;
