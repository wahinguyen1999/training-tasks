import Vue from "vue";
import VueRouter from "vue-router";
import IsAuthentication from "../utils/authen.js";
import store from "../store";

Vue.use(VueRouter);

const routes = [
  // Other routes go here...
  {
    path: "*",
    redirect: "/home",
  },
  {
    path: "/home",
    component: () => import("../layouts/Layout.vue"),
    beforeEnter: IsAuthentication,
    children: [
      {
        path: "/home",
        name: "Home",
        component: () => import("../views/Home.vue"),
        beforeEnter: IsAuthentication,
      },
    ],
  },
  {
    path: "/plan",
    name: "Plan",
    component: () => import("../layouts/Layout.vue"),
    beforeEnter: IsAuthentication,
    children: [
      {
        path: "/plan/list",
        name: "List",
        component: () => import("../views/plan/List.vue"),
      },
      {
        path: "/plan/create",
        name: "Create",
        component: () => import("../views/plan/Create.vue"),
      },
      {
        path: "/plan/edit/:id",
        name: "Edit",
        component: () => import("../views/plan/Edit.vue"),
      },
    ],
  },
  {
    path: "/contract",
    name: "Contract",
    component: () => import("../layouts/Layout.vue"),
    beforeEnter: IsAuthentication,
    children: [
      {
        path: "/contract/list",
        name: "List",
        component: () => import("../views/contract/List.vue"),
      },
      {
        path: "/contract/create",
        name: "Create",
        component: () => import("../views/contract/Create.vue"),
      },
      {
        path: "/contract/edit/:id",
        name: "Edit",
        component: () => import("../views/contract/Edit.vue"),
      },
    ],
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("../views/Login.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  routes,
});

router.beforeEach((to, from, next) => {
  // Check if the route has any query parameters
  const hasQuery = Object.keys(to.query).length > 0;

  if (!hasQuery) {
    store.commit("SET_LOADING_ROUTE", true);
  }

  next();
});

router.afterEach(() => {
  setTimeout(() => {
    store.commit("SET_LOADING_ROUTE", false);
  }, 580);
});

export default router;
