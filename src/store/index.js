import Vue from "vue";
import Vuex from "vuex";
import auth from "./auth.js";

Vue.use(Vuex);

var store = new Vuex.Store({
  state: {
    isLoadingRoute: false,
    searchQuery: "",
  },
  mutations: {
    SET_LOADING_ROUTE(state, data) {
      state.isLoadingRoute = data;
    },
    SET_SEARCH_QUERY(state, data) {
      state.searchQuery = data;
    },
  },
  modules: {
    auth: auth,
  },
});

export default store;
