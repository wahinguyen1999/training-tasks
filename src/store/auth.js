import { AuthStorage } from "@/utils/constants";
import { getLocalStorage } from "@/utils/helper";

const auth = {
  namespaced: true,
  state: {
    userData: getLocalStorage(AuthStorage),
  },
  getters: {
    accessToken: (state) => (state.userData ? state.userData.access_token : ""),
  },
  mutations: {
    SET_LOGIN(state, data) {
      state.userData = data;
    },
    SET_LOGOUT(state) {
      state.userData = null;
    },
  },
};

export default auth;
