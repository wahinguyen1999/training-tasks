// httpService.js
import axios from "axios";
import store from "../store";
import router from "../router";
import { removeLocalStorage } from "@/utils/helper";
import { AuthStorage } from "@/utils/constants";

// Base URL for the API
const BASE_API_URL =
  "https://95tq8hk6fh.execute-api.ap-northeast-1.amazonaws.com";

// Create an instance of Axios
const axiosInstance = axios.create({
  baseURL: BASE_API_URL,
  timeout: 5000, // Set the timeout for requests (optional)
  headers: {
    "Content-Type": "application/json",
  },
});

// Add an interceptor to set the authorization header before each request
axiosInstance.interceptors.request.use(
  (config) => {
    const authToken = store.getters["auth/accessToken"];
    if (authToken) {
      config.headers["Authorization"] = `Bearer ${authToken}`;
    }
    return config;
  },
  () => {}
);

// Add an interceptor when expired token
axiosInstance.interceptors.response.use(undefined, function (error) {
  if (error) {
    const originalRequest = error.config;
    if (error.response.status === 401 && !originalRequest._retry) {
      originalRequest._retry = true;
      removeLocalStorage(AuthStorage);
      store.commit("auth/SET_LOGIN", "");
      router.push("/login");
      return error;
    }
  }
});

// Common service for making HTTP requests
const httpService = {
  get(url, query = {}, config = {}) {
    // Append query parameters to the URL
    const queryString = Object.keys(query)
      .map(
        (key) => `${encodeURIComponent(key)}=${encodeURIComponent(query[key])}`
      )
      .join("&");
    const fullUrl = queryString ? `${url}?${queryString}` : url;

    return new Promise((resolve) => {
      axiosInstance
        .get(fullUrl, config)
        .then((res) => {
          if (res.status != 200) {
            resolve(res.response.data);
          }
          resolve(res.data);
        })
        .catch((err) => {
          console.log("err", err);
          resolve(err.response);
        });
    });
  },
  post(url, data, config = {}) {
    return new Promise((resolve) => {
      axiosInstance
        .post(url, data, config)
        .then((res) => {
          if (res.status != 200) {
            resolve(res.response.data);
          }
          resolve(res.data);
        })
        .catch((err) => {
          console.log("res", err);
          resolve(err.response);
        });
    });
  },
  put(url, data, config = {}) {
    return new Promise((resolve) => {
      axiosInstance
        .put(url, data, config)
        .then((res) => {
          if (res.status != 200) {
            resolve(res.response.data);
          }
          resolve(res.data);
        })
        .catch((err) => {
          console.log("err", err);
          resolve(err.response);
        });
    });
  },
  delete(url, config = {}) {
    return new Promise((resolve) => {
      axiosInstance
        .delete(url, config)
        .then((res) => {
          if (res.status != 200) {
            resolve(res.response.data);
          }
          resolve(res.data);
        })
        .catch((err) => {
          console.log("err", err);
          resolve(err.response);
        });
    });
  },
};

export default httpService;
