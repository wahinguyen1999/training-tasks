import Vue from "vue";
import Router from "vue-router";
import router from "./router";
import store from "./store";
import App from "./App.vue";
import "./styles/styles.css";

["push", "replace"].forEach((method) => {
  const originalMethod = Router.prototype[method];
  Router.prototype[method] = function m(location) {
    return originalMethod.call(this, location).catch((error) => {
      if (error.name !== "NavigationDuplicated") {
        // capture exception
      }
    });
  };
});

Vue.config.productionTip = false;
new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
